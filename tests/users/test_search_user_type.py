import pytest
from rest_framework.test import APIClient

from users.models import CustomUser, UniversityUser
from universities.models import University
from django.core.exceptions import ObjectDoesNotExist

from tests.test_utils import dicts_test_utils
from tests.test_utils import create_objects_test_utils

@pytest.mark.django_db
class TestSearchUserType:
    def setup_method(self):
        self.client = APIClient()
        self.university = University(name='UnB', cnpj='00038174000143')
        self.university.save()
        self.email_super_user = 'admin@admin.com'

        self.super_user_dict = dicts_test_utils.super_user_dict_1
        self.super_user = create_objects_test_utils.create_test_super_user(self.super_user_dict)

        self.university_admin_user_dict = dicts_test_utils.university_user_dict_1
        self.university_admin_user = create_objects_test_utils.create_test_university_admin_user(self.university_admin_user_dict, self.university)

        self.university_user_dict = dicts_test_utils.university_user_dict_2
        self.university_user = create_objects_test_utils.create_test_university_user(self.university_user_dict, self.university)


    def test_search_user_by_type_return_object(self):
        # Verificando se o usuario de teste foi criado.
        assert type(self.super_user) == CustomUser

        # Verificando se a função retorna um objeto.
        assert CustomUser.search_user_by_type(self.super_user.type) != None

    def test_search_user_by_type_check_super_user(self):
        # Verificando se o usuario de teste foi criado.
        assert type(self.super_user) == CustomUser

        # Verificando se a função retorna o tipo correto de usuario.
        assert CustomUser.search_user_by_type(self.super_user.type) == CustomUser.super_user_type

    def test_search_user_by_type_check_university_user(self):
        # Verificando se o usuario de teste foi criado.
        assert type(self.university_admin_user) == UniversityUser
        assert type(self.university_user) == UniversityUser

        # Verificando se a função retorna o tipo correto de usuario.
        assert CustomUser.search_user_by_type(self.university_admin_user.type) == UniversityUser.university_admin_user_type
        assert CustomUser.search_user_by_type(self.university_user.type) == UniversityUser.university_user_type
    
    def test_search_user_by_type_throws_exception_when_user_null(self):
        # Verificando se a função lanca a excecao.
        with pytest.raises(Exception) as e:
            CustomUser.search_user_by_type('test')

        assert "User does not exist" in str(e.value)



